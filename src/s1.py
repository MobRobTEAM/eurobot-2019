#!/usr/bin/env python3
import time
import serial
from neato_connection import Neato
from config import NEATO_PORT, ARDUINO_PORT


SHORT = 2
REGULAR = 3
LONG = 7


def violet(neato, arduino):
    print("VIOLET")
    neato.sendAsIs(b'SetLDSRotation on\n')
    time.sleep(SHORT)
    neato.sendAsIs(b'SetMotor -300 -300 300\n')
    time.sleep(REGULAR)
    neato.sendAsIs(b'SetMotor 110 110 150\n')
    time.sleep(SHORT)

    
    neato.sendAsIs(b'SetMotor 194 -194 100\n')
    time.sleep(REGULAR)
    neato.sendAsIs(b'SetMotor -150 -150 200\n')
    time.sleep(REGULAR)
    # neato.sendAsIs(b'SetMotor 0 50 200\n')
    # time.sleep(SHORT)
    neato.sendAsIs(b'SetMotor 1360 1360 300\n')
    time.sleep(LONG)
    # neato.sendAsIs(b'SetMotor 370 -370 200\n')
    # time.sleep(REGULAR)
    arduino.write(b'9')
    neato.sendAsIs(b'SetMotor 165 165 200\n')
    time.sleep(REGULAR)
    
    


def yellow(neato, arduino):
    print("YELLOW")
    neato.sendAsIs(b'SetLDSRotation on\n')
    time.sleep(SHORT)
    neato.sendAsIs(b'SetMotor -320 -320 300\n')
    time.sleep(REGULAR)
    neato.sendAsIs(b'SetMotor 94 94 300\n')
    time.sleep(SHORT)


    neato.sendAsIs(b'SetMotor 184 -184 100\n')
    time.sleep(2)
    # neato.sendAsIs(b'SetMotor 160 160 150\n')
    # time.sleep(SHORT)
    neato.sendAsIs(b'SetMotor -1390 -1390 250\n')
    time.sleep(LONG)
    # neato.sendAsIs(b'SetMotor 360 -360 100\n')
    # time.sleep(REGULAR)
    arduino.write(b'9')
    neato.sendAsIs(b'SetMotor -200 -200 100\n')
    time.sleep(REGULAR)
    

    # arduino.write(b'0')
    # time.sleep(1)
    # neato.sendAsIs(b'SetMotor 0 -10 100\n')
    # time.sleep(1)
    # neato.sendAsIs(b'SetMotor -550 -550 250\n')
    # time.sleep(REGULAR)
    # neato.sendAsIs(b'SetMotor -100 100 100\n')
    # time.sleep(REGULAR)


def arduino_start(arduino):
    print(" Wait for START...")
    reading = arduino.read()
    while not reading == b'START':
        reading = arduino.read()
        print("Got: " + str(reading))
        

def tester():
    arduino = serial.Serial(ARDUINO_PORT, baudrate=9600, timeout=1) 
    neato = Neato(NEATO_PORT)
    neato.test_mode_on()
    time.sleep(1)

    # arduino.write(b'9')
    
    violet(neato, arduino)
    # yellow(neato, arduino)

    while True:
        time.sleep(10000)

if __name__ == '__main__':
    tester()