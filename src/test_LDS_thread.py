#!/usr/bin/env python3
import time
import threading
from neato_connection import Neato
from thread_lidar import Lidar_data_collector
from config import NEATO_PORT

# neato = Neato(NEATO_PORT)
neato = ''
# neato.test_mode_on()
time.sleep(2)

# Lidar
lidar_thread = Lidar_data_collector(0, neato)
lidar_thread.start()

# Main sequence
for c in range(5000):
    time.sleep(2)
    print("main: " + str(c))

time.sleep(10)
# neato.test_mode_off()

print("Exiting Main Thread")