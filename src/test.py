#!/usr/bin/env python3
import serial
import time
from config import NEATO_PORT, ARDUINO_PORT

neato = serial.Serial(NEATO_PORT, 115200, serial.EIGHTBITS, serial.PARITY_NONE, serial.STOPBITS_ONE)

print(neato)
#print(neato.write(b'GetVersion\n'))
arduino = serial.Serial(ARDUINO_PORT, baudrate=9600, timeout=1) 

neato.write(b'TestMode on\n')
time.sleep(2)


neato.write(b'SetLDSRotation on\n')
time.sleep(2)
#neato.write(b'SetLDSRotation off\n')
# neato.write(b'SetMotor 10 10 10\n')
# time.sleep(2)
# print(str(neato.read_all(), "utf-8"))

neato.write(b'TestMode off\n')