#!/usr/bin/env python3
import threading
import math
import random
import numpy as np
import matplotlib.pyplot as plt


class Lidar_data_collector (threading.Thread):
    
    def __init__(self, threadID, neato):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = 'Lidar_data_collector'
        self.neato = neato

    
    def get_lidar_dataset(self):
        self.neato.lidar_scan_on()
        
        # str data to np array
        result = self.neato.lidar_data_read()
        for i in range(len(result)):
            result[i] = [int(num) for num in result[i][2:-5].split(',')]
        result_np = np.array(result)

        # debug reading results
        print("Thread: {}: {} - {}".format(self.name, result[0], result[-1]))
        print("Reading: {}, rows: {}".format(i, len(result)))
        # print("Error: {}".format(self.neato.get_error_info()))
        
        return result_np


    def generate_test_lidar_dataset(self):
        dataset = []
        for i in range(360):
            dataset.append([i,
            random.randint(50,90),
            random.randint(1,100),
            0])
        result_np = np.array(dataset)
        return result_np


    def lidar_data_2_plot(self, lidar_dataset):
        ax = []
        ay = []
        for row in lidar_dataset:
            x = math.cos(row[0] / 180 * math.pi) * row[1]
            y = math.sin(row[0] / 180 * math.pi) * row[1]
            # print(row[0], row[1], x, y)
            ax.append(x)
            ay.append(y)
        return ax, ay

    def test_plot_data(self):
        lidar_dataset = self.generate_test_lidar_dataset()
        ax, ay = self.lidar_data_2_plot(lidar_dataset)
        return ax, ay

    def run(self):
        print("Starting " + self.name)
        
        # lidar_dataset = self.generate_test_lidar_dataset()
        # ax, ay = self.lidar_data_2_plot(lidar_dataset)
        plt.plot(self.test_plot_data())
        plt.pause(0.5)
        plt.show()
        
        while True:
            # lidar_dataset = self.get_lidar_dataset()
            # lidar_dataset = self.generate_test_lidar_dataset()
            # ax, ay = self.lidar_data_2_plot(lidar_dataset)
            
            print("new test data " + self.name)
            plt.plot(self.test_plot_data())

