#!/usr/bin/env python3
import time
import socket
import keyboard
from config import SERVER_IP, PORT_NUMBER
from manual_control_service import ManualControlService


def move_forward(kb_event):
    # print("try move forward")
    client_socket.send(b'SetMotor -100 -100 100\n')

def move_back(kb_event):
    # print("try move back")
    client_socket.send(b'SetMotor 100 100 100\n')

def turn_right(kb_event):
    # print("try move right")
    client_socket.send(b'SetMotor 180 -180 100\n')

def turn_left(kb_event):
    # print("try move left")
    client_socket.send(b'SetMotor -180 180 100\n')

def freeze(kb_event):
    # print("try freeze")
    client_socket.send(b'SetMotor 1 1 1\n')

def set_lds_rotation_on(kb_event):
    client_socket.send(b'SetLDSRotation on\n')

def set_lds_rotation_off(kb_event):
    client_socket.send(b'SetLDSRotation off\n')

def set_motor_vacuum_on(kb_event):
    client_socket.send(b'SetMotor VacuumOn\n')

def set_motor_vacuum_off(kb_event):
    client_socket.send(b'SetMotor VacuumOff\n')

def additional_mapping():
    keyboard.on_press_key('c', freeze)
    # keyboard.on_press_key('l', set_lds_rotation_on)
    # keyboard.on_press_key('k', set_lds_rotation_off)
    # keyboard.on_press_key('v', set_motor_vacuum_on)
    # keyboard.on_press_key('b', set_motor_vacuum_off)


client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
manual_control = ManualControlService()
manual_control.map_functions(move_forward, move_back, turn_right, turn_left)
additional_mapping()
client_socket.connect((SERVER_IP, PORT_NUMBER))

def run():
    try:
        
        # print("Test client sending packets to IP {0}, via port {1}\n".format(SERVER_IP, PORT_NUMBER))
        # messages main loop
        while True:
            time.sleep(6000)
    finally:
        client_socket.close()

if __name__ == '__main__':
    run()
