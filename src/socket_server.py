#!/usr/bin/env python3
import socket
import time
import serial
from neato_connection import Neato
from config import SERVER_IP, PORT_NUMBER, NEATO_PORT


def parse_set_motor(message):
    params = str(message).split()

    lw = params[0][2:]
    rw = params[1]
    ac = params[2][:-1]

    message = 'SetMotor {} {} {}\n'.format(lw, rw, ac)
    message = bytes(message, "utf-8")
    return message

def arduino_init():
    arduino = serial.Serial('/dev/ttyACM0', baudrate=9600, timeout=1)
    while True:
        reading = arduino.read()
        print(reading)
        # print ( hello  ==  unicode ( "hello \ n " )) b'0'
    return arduino


server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)         
server_socket.bind((SERVER_IP, PORT_NUMBER))
print ("Server listening on IP {0}, port {1}\n".format(SERVER_IP, PORT_NUMBER))
server_socket.listen(10)


try:
    neato = Neato(NEATO_PORT)
    neato.test_mode_on()

    connection, client_address = server_socket.accept()
    print('Got connection from', client_address)
    connection.send(b'Thank you for connecting')

    while True:
        message = connection.recv(1024)
        if message != b'':
            print('send: ', str(message))
            neato.sendAsIs(message) 
finally:
    connection.close()
    neato.test_mode_off()