#!/usr/bin/env python3
import keyboard


class ManualControlService:
    def __init__(self, forward = 'w', backward = 's', right = 'd', left = 'a'):
        # key bindings
        self._MOVE_FORWARD_KEY = forward
        self._MOVE_BACK_KEY = backward
        self._TURN_RIGHT_KEY = right
        self._TURN_LEFT_KEY = left

    # Input parameters ARE FUNCTIONS: move_forward, move_back, turn_right, turn_left
    def map_functions(self, move_forward, move_back, turn_right, turn_left):
        keyboard.on_press_key(self._MOVE_FORWARD_KEY, move_forward)
        keyboard.on_press_key(self._MOVE_BACK_KEY, move_back)
        keyboard.on_press_key(self._TURN_RIGHT_KEY, turn_right)
        keyboard.on_press_key(self._TURN_LEFT_KEY, turn_left)

    def debug_mode(self, mode_on):
        keyboard.on_press(self.key_info)

    def key_info2file(self, kb_event):
        with open('debug_keyboard.txt','a') as f:
            f.write("pressed name: ", kb_event.name)
            f.write("pressed code: ", kb_event.scan_code)

    def key_info(self, kb_event):
        print("pressed name: ", kb_event.name)
        print("pressed code: ", kb_event.scan_code)
