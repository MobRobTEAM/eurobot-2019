#!/usr/bin/env python3
import serial 
import re
import time


class Neato:
    def __init__(self, port = '/dev/ttyACM0',
    baudrate = 115200,
    bytesize = serial.EIGHTBITS,
    parity = serial.PARITY_NONE,
    stopbits = serial.STOPBITS_ONE):
        self._port = port
        self._baudrate = baudrate
        self._bytesize = bytesize
        self._parity = parity
        self._stopbits = stopbits
        self.connect()

    def connect(self):
        # robot = serial.Serial('/dev/ttyACM0', 115200, serial.EIGHTBITS, serial.PARITY_NONE, serial.STOPBITS_ONE)
        self._neato = serial.Serial(
            self._port,
            self._baudrate,
            self._bytesize,
            self._parity,
            self._stopbits
        )
        print(self._neato)

    def test_mode_on(self):
        self._neato.write(b'TestMode on\n')

    def test_mode_off(self):
        self._neato.write(b'TestMode off\n')

    def get_version(self):
        self._neato.write(b'GetVersion\n')
        return str(self._neato.read_all(), "utf-8")

    def get_charger_info(self):
        self._neato.write(b'GetCharger\n')
        return str(self._neato.read_all(), "utf-8")

    def get_life_stat_log(self):
        self._neato.write(b'GetLifeStatLog\n')
        return str(self._neato.read_all(), "utf-8")

    def sound_check(self):
        self._neato.write(b'PlaySound 6\n')
        time.sleep(2)
        self._neato.write(b'PlaySound 6 Stop\n')
        # self._neato.write(b'PlaySound Stop\n')

    def sendAsIs(self, message):
        self._neato.write(message)

    def send(self, message):
        message = message + '\n'
        message = bytes(message, "utf-8")
        self._neato.write(message)

    def set_motor(self, lw, rw, ac):
        message = 'SetMotor {} {} {}\n'.format(lw, rw, ac)
        message = bytes(message, "utf-8")
        self._neato.write(message)

    def lidar_scan_on(self):
        self._neato.write(b'SetLDSRotation on\n')
        self._neato.write(b'SetLDSScan on\n')

    def lidar_data_read(self):
        self._neato.write(b'GetLDSScan\n')
        data_collection = []
        data = str(self._neato.readline())
        while not re.match("b'AngleInD*", data):
            data = str(self._neato.readline())
        data = str(self._neato.readline())
        while not re.match("b'ROTATION*", data):
            data_collection.append(data)
            data = str(self._neato.readline())
        
        return data_collection
        
    def get_error_info(self):
        self.send('GetErr')
        return str(self._neato.read_all())