#!/usr/bin/env python3
import time
from manual_control_service import ManualControlService
from neato_connection import Neato
from config import NEATO_PORT

manual_control = ManualControlService()

def move_forward(kb_event):
#     print("try move forward") 
    neato.set_motor(100, 100, 100)   

def move_back(kb_event):
#     print("try move back")
    neato.set_motor(-100, -100, 100)   

def turn_right(kb_event):
#     print("try move right")
    neato.set_motor(-100, 100, 100)   

def turn_left(kb_event):
#     print("try move left")
    neato.set_motor(100, -100, 100)   

manual_control.map_functions(move_forward, move_back, turn_right, turn_left)
neato = Neato(NEATO_PORT)
neato.test_mode_on()

# main loop
while True:
    time.sleep(600)
    
neato.test_mode_off()

'''
# sample file output
def file_output(message):
    with open('output_buffer.txt','a') as f:
        f.write(message)
'''

'''
# sample handler
def move_forward(kb_event):
    print("try move forward")
    # robot.write(b'SetMotor -1000 -1000 100\n')
'''

'''
sample main loop
def run():
    try:
        print("Turning on TestMode...")
        # print(robot.flush())
        # neato_connection.test_mode_on(robot)
        print("TestMode on")
        while True:
            time.sleep(600)
    finally:
        print("Finally block")
        # neato_connection.test_mode_off(robot)
'''
